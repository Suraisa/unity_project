﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowHero : MonoBehaviour {

	public Transform hero;

	void FixedUpdate()
	{
		if (GameObject.FindWithTag("Hero")==true){
			transform.position = new Vector3(hero.position.x, hero.position.y,-10);
		}
	}
}
