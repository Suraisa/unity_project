﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlopEnemy : Character {
	public Transform heroPosition;
	
	bool WallFound(){
		RaycastHit2D hit;
		if (rb2d.velocity.x>0){
			hit = Physics2D.Raycast(transform.position, Vector2.right,0.6f);
		}
		else{
			hit = Physics2D.Raycast(transform.position, Vector2.left,0.6f);
		}
		if(hit.collider){
			if (hit.collider.gameObject.tag == "Wall"){
				return true;
			}
		}
		return false;
	}	

	void Start () {
		currentHp = 100;
		speed = 5;
		rb2d = GetComponent<Rigidbody2D>();
		animationState = GetComponent<Animator>();
		coll = GetComponent<Collider2D>();
		shootCooldown = 0f;
		shootingInterval = 1.5f;
		float initTime = Time.time;
		heroPosition = GameObject.FindWithTag("Hero").GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		if (shootCooldown > 0){
			shootCooldown -= Time.deltaTime;
		}
		if (GameObject.FindWithTag("Hero")==true){
			if (heroPosition.position.x<transform.position.x){
				Movement(-1);
				animationState.SetBool("Right", false);
			}
			else{
				Movement(1);
				animationState.SetBool("Right", true);
			}
		}

	}
	void FixedUpdate(){
		if (GameObject.FindWithTag("Hero")==true){
			if(heroPosition.position.y<transform.position.y+0.5f && heroPosition.position.y>transform.position.y-0.5f && shootCooldown<=0){
				Fire(true);
			}
			else{
				Fire(false);
			}
			if(heroPosition.position.y>transform.position.y+1.5f || WallFound()){
				Jump(true);
			}
		}
		else{
			Jump(false);
		}
	}
}
