﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {
	public GameObject enemy;

	void Start()
	{
		Instantiate(enemy);
	}

	void Update()
	{
		if(GameObject.FindWithTag("Enemy")==false){
			Instantiate(enemy);
		}
	}

}
