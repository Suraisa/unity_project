﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlopHero : Character {
	float hpMax = 100f;
	public Slider healthBar;
	public Text healthText;
	public int money = 0;
	public Text moneyText;

	public float PercentageHealth(){
		return currentHp/hpMax;
	}

	// Use this for initialization
	void Start () {
		currentHp = hpMax;
		speed = 10;
		rb2d = GetComponent<Rigidbody2D>();
		animationState = GetComponent<Animator>();
		coll = GetComponent<Collider2D>();
		//soundSource.clip = jumpSound;
		healthBar.value = PercentageHealth();
		healthText.text = currentHp.ToString()+"Hp";
		shootCooldown = 0f;
		shootingInterval = 0.5f;
		animationState.SetBool("Right", true);
		float initTime = Time.time;

	}
	
	// Update is called once per frame
	void Update () {
		if (shootCooldown > 0){
			shootCooldown -= Time.deltaTime;
		}
		if (!Input.GetKey("right") && !Input.GetKey("left")){
			Movement(0);
		}
		if (!Input.GetKey("space") && OnTheGround()==true){
			Jump(false);
		}
		if(!Input.GetKey("z")){
			Fire(false);
		}
	}

	void FixedUpdate() {
		if (Input.GetKey("right")){
			Movement(1);
			animationState.SetBool("Right", true);
		}
		else if (Input.GetKey("left")){
			Movement(-1);
			animationState.SetBool("Right", false);
		}
		if (Input.GetKeyDown("space")){
			Jump(true);
		}
		if (Input.GetKeyDown("z") && shootCooldown<=0){
			Fire(true);
		}
	}
}
