﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Character : MonoBehaviour {
	public float currentHp;
	public float speed;
	public Rigidbody2D rb2d;
	public Animator animationState;
	public Collider2D coll;
	public float yOriginPosition = 0.618f;
	public AudioSource soundSource;
	public Transform bulletPrefab;
	public float shootingInterval;
	public float shootCooldown;
	public Vector3 rightBulletPosition = new Vector3(0.6f,-0.06f,0);
	public Vector3 leftBulletPosition = new Vector3(-0.6f,-0.06f,0);
	public float initTime;
	public float fireTime=0.7f;
	public AudioClip jumpSound;
	public AudioClip shotSound;
	public float jumpHeight = 20f;
	public void Movement (int move){
		if (move == 1){
			rb2d.velocity = Vector2.right * speed + new Vector2(0,rb2d.velocity.y);
			animationState.SetInteger("Direction", move);
			animationState.SetBool("Right", true);
		}
		else if (move == -1){
			rb2d.velocity = Vector2.left * speed + new Vector2(0,rb2d.velocity.y);
			animationState.SetInteger("Direction", move);
			animationState.SetBool("Right", false);
		}
		else{
			rb2d.velocity = new Vector2(0,rb2d.velocity.y);
			animationState.SetInteger("Direction", move);
		}
	}

	public bool OnTheGround(){
		RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down,0.6f);
		if (hit.collider){
			if (hit.collider.gameObject.tag == "Wall"){
				return true;
			}
		}
		return false;
	}

	public void Jump(bool jump){
		if (OnTheGround()==true && jump == true){
				animationState.SetBool("Jump", jump);
				rb2d.velocity = new Vector2(rb2d.velocity.x, jumpHeight);
				SoundManager.Instance.PlaySound(jumpSound);
		}
		else{
			animationState.SetBool("Jump", false);
		}
	}

	public void Fire(bool fire){
		if(Time.time >= initTime + fireTime){
			animationState.SetBool("Fire",false);
			initTime=Time.time;
		}
		if (shootCooldown <=0f && fire){
			shootCooldown = shootingInterval;
			var bulletTransform = Instantiate(bulletPrefab) as Transform;
			BulletMovement bulletMovement = bulletTransform.gameObject.GetComponent<BulletMovement>();
			if (animationState.GetBool("Right")){
				animationState.SetBool("Fire", fire);
				bulletMovement.direction = Vector2.right;
				bulletTransform.position = transform.position + rightBulletPosition;
			}
			else if (!animationState.GetBool("Right")){
				animationState.SetBool("Fire",fire);
				bulletMovement.direction = Vector2.left;
				bulletTransform.position = transform.position + leftBulletPosition;
			}
			SoundManager.Instance.PlaySound(shotSound);
		}
	}
}
