﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Money : MonoBehaviour {
	public int money = 20;
	public AudioClip moneySound;

	void OnTriggerEnter2D(Collider2D other)
	{	
		if (other.gameObject.tag == "Hero"){
			BlopHero hero = other.gameObject.GetComponent<BlopHero>();
			hero.money += money;
			hero.moneyText.text = hero.money.ToString()+"$";
			SoundManager.Instance.PlaySound(moneySound);
			Destroy(gameObject);
		}
	}
}
