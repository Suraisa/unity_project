﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletShot : MonoBehaviour {
	public int damage = 20;
	public Transform moneyPrefab;
	public AudioClip heroDieSound;
	public AudioClip enemyDieSound;
	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.tag != "EnemyBullet" || other.gameObject.tag != "HeroBullet"){
			if (other.gameObject.tag == "Hero" && gameObject.tag == "EnemyBullet"){
				BlopHero hero = other.gameObject.GetComponent<BlopHero>();
				hero.currentHp -= damage;
				hero.healthBar.value = hero.PercentageHealth();
				hero.healthText.text = hero.currentHp.ToString()+"Hp";
				Destroy(gameObject);
				if(hero.currentHp<=0){
					Destroy(hero.gameObject);
					SoundManager.Instance.PlaySound(heroDieSound);
				}
			}
			else if(other.gameObject.tag == "Enemy" && gameObject.tag == "HeroBullet"){
				BlopEnemy enemy = other.gameObject.GetComponent<BlopEnemy>();
				enemy.currentHp -= damage;
				Destroy(gameObject);
				if(enemy.currentHp<=0){
					Transform enemyPosition = other.gameObject.GetComponent<Transform>();
					var moneyTransform = Instantiate(moneyPrefab) as Transform;
					moneyTransform.position = enemyPosition.position - new Vector3(0,0.5f,0);
					Destroy(enemy.gameObject);
					SoundManager.Instance.PlaySound(enemyDieSound);
				}
			}
		}
		if (other.gameObject.tag == "Wall"){
			Destroy(gameObject);
		}
	}

	void Start () {
		Destroy(gameObject,20);
	}

}
