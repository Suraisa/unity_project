﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour {
	public Vector2 direction = Vector2.right;
	public float speed = 1f;
	public Rigidbody2D rb2d;
	// Use this for initialization
	void Start () {
		rb2d = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		rb2d.AddForce(direction*speed,ForceMode2D.Impulse);
	}
}
