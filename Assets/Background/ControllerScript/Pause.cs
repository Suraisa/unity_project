﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour {
	public bool paused;
	// Use this for initialization
	void Start () {
		paused = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("p")){
			paused = !paused;
		}
		if (paused){
			Time.timeScale = 0;
		}
		else{
			Time.timeScale = 1;
		}
	}
	public void pause(){
		paused = !paused;
		if (paused){
			Time.timeScale = 0;
		}
		else{
			Time.timeScale = 1;
		}
	}
}
