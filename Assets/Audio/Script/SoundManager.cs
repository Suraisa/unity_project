﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
        public AudioSource Sound;
		private static SoundManager instance;
        public static SoundManager Instance { get{return instance;}} 

		void Awake ()
	{
		if (instance != null && instance != this)
			Destroy (gameObject);
		instance = this;
	}

	public void PlaySound(AudioClip clip){
		Sound.clip = clip;
		Sound.Play();
	}

}